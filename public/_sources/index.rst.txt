.. Kassow CBunX Framework documentation master file, created by
   sphinx-quickstart on Wed Oct 26 01:37:01 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CBunX documentation!
===============================

The **CBunX framework** extends the `CBun framework <https://docs.kassowrobots.com/en/cbuns/development/Introduction>`_, allowing developers to enhance their CBuns with fully custom UIs. To see CBunX in action, check out the following video:

..  youtube:: ExP_Ru1K324
   :width: 100%

|

Build on the Android SDK, CBunX leverages modern, intuitive technologies, including **Kotlin**, **Jetpack Compose**, and **Coroutines**, giving developer a powerful toolkit for creating seamless and responsive user interfaces. 

Additionally, Kassow Robots has introduced two Android libraries within the CBunX framework: 

* :ref:`cbunx-core`: an essential building block of each CBunX frontend
* :ref:`kr-composables`: a collection of Kassow Robots-styled composables for Jetpack Compose 

.. note:: 
   Both Kassow Robots libraries are now available through the `Maven Central Repository <https://central.sonatype.com/namespace/com.kassowrobots>`_.

Contents
--------

These documents guide you in building CBun frontend using the CBunX API, alongside Android framework APIs and other libraries. 


.. toctree::
   :caption: Table of Contents

   Home <self>
   introduction/introduction
   use_cases/use_cases
   first_cbun_app/first_cbun_app
   design_guide/design_guide
   advanced_topics/advanced_topics
   api/api
   examples/examples
   faq/faq

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
