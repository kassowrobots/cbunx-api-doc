package com.kassowrobots.designguide

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.toColorInt
import com.kassowrobots.api.app.fragment.KRFragment

class TextFieldExampleFragment : KRFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(
                ViewCompositionStrategy.DisposeOnLifecycleDestroyed(viewLifecycleOwner)
            )
            setContent {
                Surface(color = Color("#FFE2E6F0".toColorInt())) {
                    Column(modifier = Modifier.padding(20.dp)) {
                        Heading2(text="Text Input")

                        var input1 by remember { mutableStateOf("Hello World!") }
                        TextInput(
                            value = input1,
                            onValueChangeCallback = { newValue ->
                                input1 = newValue
                            }
                        )

                        Spacer(modifier = Modifier.height(5.dp))

                        var input2 by remember { mutableStateOf("Hello Robot!") }
                        TextInput(
                            value = input2,
                            onValueChangeCallback = { newValue ->
                                input2 = newValue
                            },
                            enabled = false
                        )
                    }
                }
            }
        }
    }

    @Composable
    fun Heading2(text: String, modifier: Modifier = Modifier) {
        Text(
            text=text.uppercase(),
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
            color = Color("#FF393B3F".toColorInt()),
            modifier = modifier
        )
        Spacer(modifier = Modifier.height(12.dp))
    }

    @Composable
    fun TextInput(
        value: String,
        onValueChangeCallback: (String) -> Unit,
        modifier: Modifier = Modifier,
        enabled: Boolean = true
    ) {
        val focusManager = LocalFocusManager.current
        BasicTextField(
            value = value,
            modifier = modifier
                .height(50.dp)
                .background(
                    color = Color.White,
                    shape = RoundedCornerShape(5.dp)
                )
                .padding(16.dp),
            onValueChange = {
                onValueChangeCallback(it)
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    focusManager.clearFocus(false)
                }
            ),
            textStyle = TextStyle.Default.copy(
                fontSize = 14.sp,
                color = if (enabled) Color("#FF393B3F".toColorInt()) else Color("#FF9A9A9A".toColorInt()),
                fontFamily = FontFamily.Monospace
            ),
            enabled = enabled
        )
    }

}