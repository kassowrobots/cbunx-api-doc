package com.kassowrobots.designguide

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.toColorInt
import com.kassowrobots.api.app.fragment.KRFragment

class TextExampleFragment : KRFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(
                ViewCompositionStrategy.DisposeOnLifecycleDestroyed(viewLifecycleOwner)
            )
            setContent {
                Surface(color = Color("#FFE2E6F0".toColorInt())) {
                    Column(modifier = Modifier.padding(20.dp)) {
                        Heading1("Heading1")
                        Heading2(text="Heading2")
                        Heading3(text="Heading3")
                        Content(text="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras elementum. Donec iaculis gravida nulla. Duis condimentum augue id magna semper rutrum. Nunc tincidunt ante vitae massa. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Mauris dictum facilisis augue. Mauris metus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Fusce nibh.")
                    }
                }
            }
        }
    }

    @Composable
    fun Heading1(text: String, modifier: Modifier = Modifier) {
        Text(
            text=text,
            fontSize = 35.sp,
            color = Color("#FF393B3F".toColorInt()),
            modifier = modifier
        )
        Spacer(modifier = Modifier.height(20.dp))
    }

    @Composable
    fun Heading2(text: String, modifier: Modifier = Modifier) {
        Text(
            text=text.uppercase(),
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
            color = Color("#FF393B3F".toColorInt()),
            modifier = modifier
        )
        Spacer(modifier = Modifier.height(12.dp))
    }

    @Composable
    fun Heading3(text: String, modifier: Modifier = Modifier) {
        Text(
            text=text,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = Color("#FF616265".toColorInt()),
            modifier = modifier
        )
        Spacer(modifier = Modifier.height(10.dp))
    }

    @Composable
    fun Content(text: String, modifier: Modifier = Modifier) {
        Text(
            text=text,
            fontSize = 13.sp,
            color = Color("#FF616265".toColorInt()),
            modifier = modifier
        )
    }
    
}